package nl.utwente.di.translate;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class Translate extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Translator translator;
	
    public void init() throws ServletException {
    	translator = new Translator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius to Fahrenheit translator";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   Double.toString(translator.getFahrenheit(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
  }
  

}
