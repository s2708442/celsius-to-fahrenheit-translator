package nl.utwente.di.translate;

public class Translator {

    public double getFahrenheit(String Celsius) {
        return (Double.parseDouble(Celsius) * 1.8) + 32.0;
    }
}
