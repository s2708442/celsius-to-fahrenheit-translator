package nl.utwente.di.translate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTranslator {
    @Test
    public void testBook1 () throws Exception {
        Translator translator = new Translator();
        double Fahrenheit = translator.getFahrenheit("30.0");
        Assertions.assertEquals(86.0 , Fahrenheit, 0.0 ,"Degree in Fahrenheit") ;
    }

}
